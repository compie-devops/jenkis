# Instructions
1. Create key, secret and refresh_token from bitbucket workspace and add them to jenkins Credentials as secret texts.
2. Create SSH key ($ssh-keygen) on private instance and add the puublic key to your bitbucket account. (this is for ansible checkouts on the private instance)


## 1. Get API Token
1. Create Key & Secret at Workspace >> Settings >> OAuth consumers >> Add consumer.
```diff
# From UI Grab Key & Secret.
key=""
secret=""
```
2.  Insert your to this follow url and grab the code. 
```diff
https://bitbucket.org/site/oauth2/authorize?client_id={insert_your_key}&response_type=code
```
3. Insert your Key & Secret & code to this command and grab the refresh_token.
```diff
curl -X POST -u "{insert_key}:{insert_secret}" \
  https://bitbucket.org/site/oauth2/access_token \
  -d grant_type=authorization_code -d code={insert_code}
```
