#!/bin/bash
repository=$1
branch=$2
dst="/home/ubuntu/repositories/$repository"
cat <<EOF >> ansible/playbook.yaml
  - name: Checkout
    ansible.builtin.git:
      repo: "git@bitbucket.org:7chairs/$repository.git"
      dest: $dst
      accept_hostkey: yes
      key_file: /home/ubuntu/.ssh/maagalim_asg_allinone_demo
      version: $branch
EOF
